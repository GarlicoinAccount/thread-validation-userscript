# Thread Validation Userscript

A userscript for validating threads at r/counting

## Installation
1. [Install Tampermonkey](https://tampermonkey.net)
2. [Install the script](https://gitlab.com/GarlicoinAccount/thread-validation-userscript/raw/master/Thread%20Validator.user.js)

## Usage
1. Find the thread  you want to validate and the page from which you want to validate.
2. Change the URL from `https://www.reddit.com/etc/etc/etc/` to `https://tv.reddit.com/etc/etc/etc/`
3. If the script encounters a problem, it'll show a dialog. Copy the URL in the address bar to notepad and the message in the dialog as well, [like this](https://pastebin.com/raw/58hFkxVW)
  a. If the script is about to go to the wrong page because the top comment is some conversation instead of a count, stop it by spamming `Esc` repeatedly.

### Important stuff
1. If the script appears to do nothing, the thread is probably not supported yet. You might be able to get it to work by just adding an `// @match` at the top.
2. The script will show the deviation from the correct count when it reaches the last page of the thread. If this does not happen, but the script functioned correctly otherwise, it means there were no miscounts.
3. The script compares the top comment at the previous page against the top comment at the current page. If you're at the last page, it won't catch mistakes in the comments below the top comment.
4. Don't use the back button, it'll generate false 'miscount' reports

### Fixing miscounts
When you leave a comment to correct for miscounts, you can use the URLs you copied to notepad and add `&context=10`; the miscount will be one of the comments at that page. (You'll probably want to disable the script or remove the 'tv.' from the URLs before you do this.)

## Disclaimer
This script has only been shown not to engage in intergalactic warfare when run in [Firefox](https://getfirefox.com) + Tampermonkey. Usage with any other browsers / userscript managers is entirely at your  own risk. <sup><sup>It'll probably just work fine though</sup></sup>