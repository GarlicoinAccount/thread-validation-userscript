// ==UserScript==
// @name         r/counting THREAD VALIDATOR
// @namespace    http://tampermonkey.net/
// @author       u/GarlicoinAccount
// @license      Apache2
// @version      0.27
// @description  Tested with many bases
// @downloadURL  https://gitlab.com/GarlicoinAccount/thread-validation-userscript/-/raw/master/Thread%20Validator.user.js
// @match        https://tv.reddit.com/r/counting/comments/*/*k_counting_thread/*
// @match        https://tv.reddit.com/r/counting/comments/nk3z95/post_on_the_public_subreddit_known_as_rcounting/*
// @match        https://tv.reddit.com/r/counting/comments/*/*k_count/*
// @match        https://tv.reddit.com/r/counting/comments/*/*k_counting_thread_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*000_counting_thread/*
// @match        https://tv.reddit.com/r/counting/comments/*/*_counting_thread/*
// @match        https://tv.reddit.com/r/counting/comments/*/decimal_*k/*
// @match        https://tv.reddit.com/r/counting/comments/*/decimal_*000/*
// @match        https://tv.reddit.com/r/counting/comments/*/main_counting_thread_decimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/counting_*k/*
// @match        https://tv.reddit.com/r/counting/comments/*/regular_counting_*/*
// @match        https://tv.reddit.com/r/ButtonAftermath/comments/*/hello_everyone/*
// @match        https://tv.reddit.com/r/counting/comments/*/wait_*_posts_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/wait_2_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/wait_3_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/wait_9_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*throwaway_accounts_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/sheep_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/even_slower_counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/slower_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/count_with_sums_of_4_or_less_square_numbers_*/*
// @match        https://tv.reddit.com/r/counting/comments/8t77k9/prime_factorization_18000/*
// @match        https://tv.reddit.com/r/counting/comments/*/length_in_meters_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/meters_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/slow_counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/slow_*/*
// @match        https://tv.reddit.com/r/lounge/comments/8nkyvm/i_need_help_procrastinating_5/*/*
// @match        https://tv.reddit.com/r/counting/comments/9t48sl/square_numbers_10002_1000000/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_13*/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_14*/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_32_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/california_license_plates_0aa*/*
// @match        https://tv.reddit.com/r/counting/comments/*/dozenal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_*s*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_1*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_2*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_3*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_4*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_5*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_6*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_7*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_8*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*count*_by_9*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*by_10s*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*by_99s*/*
// @match        https://tv.reddit.com/r/counting/comments/*/by_*s_*/*
// @match        https://tv.reddit.com/r/ButtonAftermath/comments/94d092/hello/*
// @match        https://tv.reddit.com/r/notinteresting/comments/98grsd/numbers_1100_arranged_in_numerical_order/*
// @match        https://tv.reddit.com/r/counting/comments/5a1hs9/counting_in_increments_of_10_going_up_10_numbers/*
// @match        https://tv.reddit.com/r/counting/comments/5a1hs9/counting_in_increments_of_10_going_up_10_numbers/*
// @match        https://tv.reddit.com/r/counting/comments/807wty/we_havent_done_binary_coded_decimal_yet_its_about/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_27*/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_23*/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_21*/*
// @match        https://tv.reddit.com/r/counting/comments/*/base_17*/*
// @match        https://tv.reddit.com/r/counting/comments/*/unvigesimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/9u8n2k/binary_coded_decimal_revival/*
// @match        https://tv.reddit.com/r/counting/comments/*/binarycoded_decimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/9uctfk/sexvigintesimal_base_27/*
// @match        https://tv.reddit.com/r/counting/comments/9ahmxh/counting_while_writing_a_story_together_the_sixth/*
// @match        https://tv.reddit.com/r/counting/comments/*/vigesimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/binary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/ternary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/quaternary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/quinary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/septenary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/octal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/nonary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/hexadecimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/alphanumerics_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/alphanumeric_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/temperature_fahrenheit*/*
// @match        https://tv.reddit.com/r/counting/comments/*/undecimal_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/count_once_per_thread*/*
// @match        https://tv.reddit.com/r/counting/comments/*/*photoshop*/*
// @match        https://tv.reddit.com/r/counting/comments/*/backwards_counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/backwards_*000/*
// @match        https://tv.reddit.com/r/counting/comments/*/negative_numbers_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/slowestest_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/slowerest_counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/letters_*/*
// @match        https://tv.reddit.com/r/MegaLounge/comments/*/the_megalounge_counting_thread_*/*
// @match        https://tv.reddit.com/r/testcounting/comments/*/counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/street_view_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/everything_is_four_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/only_double_counting_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/william_the_conqueror_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/goldbach_conjecture_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/kenobinary_*/*
// @match        https://tv.reddit.com/r/counting/comments/*/coloured_squares*/*
// @match        https://tv.reddit.com/r/counting/comments/*/colored_squares*/*
// @grant        none
// ==/UserScript==


(function() {
    'use strict';

    //For compatibilty with threads that don't sort by old
    if(location.search.indexOf("sort=old") === -1 && document.querySelector('body div.content div.commentarea div.menuarea div.spacer div.dropdown.lightdrop span.selected').textContent.substring(0,3) !== 'old'/*Second check only works when language is set to English, other langages get an unnecessary redirect*/){
        return location.replace(location.href.replace(/(\?|$)/, "?sort=old&"));//Ensure thread is sorted by old - also done in deepthread code so one HTTP request less
    }

    const BASE = +(location.pathname.match(/base_(\d+)/) && location.pathname.match(/base_(\d+)/)[1] || location.pathname.match(/alphanumerics?_/) && 36 || location.pathname.match(/letters_/) && 26 ||
                  location.pathname.match(/unvigesimal/) && 21 || location.pathname.match(/vigesimal/) && 20 || location.pathname.match(/hexadecimal/) && 16 || location.pathname.match(/dozenal/) && 12 ||
                  location.pathname.match(/undecimal/) && 11 || location.pathname.match(/nonary|colou?red_squares/) && 9 || location.pathname.match(/octal/) && 8 || location.pathname.match(/septenary/) && 7 ||
                  location.pathname.match(/quinary/) && 5 || location.pathname.match(/quaternary/) && 4 || location.pathname.match(/ternary/) && 3 || location.pathname.match(/quaternary/) && 4 ||
                  location.pathname.match(/binary_\d|kenobinary_/) && 2 || 10);
    const INCREMENT_BY = +(location.pathname.match(/(?:by|in_increments_of)_(\d+)/) && location.pathname.match(/(?:by|in_increments_of)_(\d+)/)[1] ||
                           location.pathname.match(/(?:negative_numbers|backwards_counting)_/) && -1 || location.pathname.match(/goldbach_conjecture_/) && 2 || 1);//In decimal

    if(document.title === "reddit broke!") return setTimeout(()=>location.reload(), Math.floor(Math.random() * 1000 * 45));

    var currentComment = document.querySelector('body .content .commentarea [data-type="comment"] .entry [id^="form-t1_"] .usertext-body.md-container .md').textContent;

    //Special fix for https://tv.reddit.com/r/counting/comments/9t48sl/square_numbers_10002_1000000/
    if(location.pathname.indexOf('/square_numbers')>-1){
        currentComment = currentComment.replace(/(\d{3})2/,'$1');

    //Binary coded decimal - convert the binary to decimal first
    }else if(location.pathname.indexOf('binary_coded_decimal')>-1 || location.pathname.indexOf('binarycoded_decimal')>-1){
        currentComment = currentComment.replace(/([01]{4})/g,nybble=>parseInt(nybble,2));

    //Kenobinary - just replace 'hello' with 1 and 'there' with 0
    }else if(location.pathname.indexOf('kenobinary_')>-1){
        currentComment = currentComment.replace(/\s*hello/ig, 1).replace(/\s*there/ig, 0);

    //Letters - convert to base 26 first
    //Note: this is actually not 100% base 26 because in Letters we have AA, AAA, AAAA etc. which never happen in base 26 (they'd be 00, 000, 0000 etc.); this is only a problem in threads where we gain an extra letter in length
    }else if(location.pathname.indexOf('letters_')>-1){
        currentComment = currentComment.replace(/([A-Z])/g,letter=>"ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(letter).toString(BASE).toUpperCase());

    //Coloured squares - is base 9, just replace the squares with the corresponding digits
    }else if(location.pathname.indexOf('coloured_squares')>-1 || location.pathname.indexOf('colored_squares')>-1){
        currentComment = currentComment.replace(/\uFE0F/ug,''/*\uFE0F: some keyboards insert the Variation Selector-16 (U+FE0F) after a square, which is used to force emoji presentation*/)
            .replace(/([⬛🟫🟪🟦🟩🟨🟧🟥⬜])/ug,colour=>[..."⬛🟫🟪🟦🟩🟨🟧🟥⬜"/*Convert to Array because "string".indexOf isn't unicode-aware.*/].indexOf(colour).toString(BASE).toUpperCase());
    }

    //Reddit sometimes glitches and has a deepthread link after only 6 comments. Reloading (sometimes a few times) will make it go away
    if(
        document.querySelector`
div.content >
div.commentarea >
div[id^=siteTable_t3_].sitetable.nestedlisting >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.morerecursion >
div.entry >
span.deepthread >
a`
   ){
        return location.reload()
    }

    var currentCount = parseInt(currentComment.replace(/^\s*(?:\/?u\/throwaway|0AA[A-Z])/i,'').replace(INCREMENT_BY%1===0?/[\.  ,🐑]/ug:/[  ,🐑]/ug, ''), BASE);//Might cause confusion with commentary text if it's directly after the count (space-separated, not newline-separated)

    if(isNaN(currentCount)){
        currentCount = previousCount + 9*INCREMENT_BY;
        alert("Invalid count!!! (Treating it as " + currentCount + ")\n\n"+currentComment);
    }

    var previousCount;

    //PREVIOUS COUNT IS ALWAYS STORED AS BASE 10!
    if(typeof sessionStorage['previousCount_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] !== 'undefined'){
        previousCount = parseInt (sessionStorage['previousCount_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]], 10); //PREVIOUS COUNT IS ALWAYS STORED AS BASE 10!

        var deviation = currentCount - previousCount - 9*INCREMENT_BY;

        //Check last count vs previous count
        if(deviation !== 0){

            var combinedDeviation = +(sessionStorage['combinedDeviation_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] || 0) + deviation;

            alert("MISCOUNT!!\n\nCurrent count is "+ currentCount.toString(BASE) + ", should be " + (previousCount + 9*INCREMENT_BY).toString(BASE) + "\n\nDeviation: " + deviation + "\nCombined deviation: " + combinedDeviation);

            sessionStorage['combinedDeviation_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] = combinedDeviation;
        }

        //Note that we'll always continue from the current count even if it's invalid!
        sessionStorage['previousCount_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] = currentCount;//ALREADY IN BASE 10!

    }else{//We've just began our checking run, so we'll only store the comment for comparison and continue
        sessionStorage['previousCount_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] = currentCount;//ALREADY IN BASE 10!
    }

    /*Handle clicking the deepthread links*/
    /*Only ever follows the deepthread link if every comment is the first child of its parent (to prevent following late chains that are longer)*/
    let deepthread = document.querySelector`
div.content >
div.commentarea >
div[id^=siteTable_t3_].sitetable.nestedlisting >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.comment:first-child >
div.child >
div[id^=siteTable_t1_].sitetable.listing >
div[id^=thing_t1_].thing.morerecursion >
div.entry >
span.deepthread >
a`,
        deepthreads = document.querySelectorAll('.deepthread a');//So we can detect late chains or if we're stuck on off-topic chatter

    if(deepthread){
        if(deepthreads.length > 1){
            focus();
            alert("Are others on late chain? (other deepthread links detected)");
        }
        localStorage['last_new_comment_page_time_' + location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1] ] = Date.now(); // So we won't refresh as often for less busy threads
        location.href = deepthread.href.replace(/\?|$/, '?sort=old&');
    }else if(deepthreads.length > 0){
        focus();
        alert("Stuck on off-topic chatter? (Or others on late chain)");
    }else{
        if(typeof sessionStorage['combinedDeviation_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]] !== "undefined"){
            alert("Combined deviation: " + sessionStorage['combinedDeviation_'+location.pathname.match(/^\/r\/counting\/comments\/([^\/]+)/)[1]]);
            delete sessionStorage['combinedDeviation_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]];
        }

        delete sessionStorage['previousCount_'+location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1]];
        setTimeout(()=>location.reload(), getTimeoutLength());
    }
    /**
     * Function for calculating cooldown
     *
     * If there are multiple validation scripts open at the same time, having them all refresh every 30 seconds is going to take quite some CPU and bandwith, so
     * we refresh less frequently if the latest activity was longer ago
     */
    function getTimeoutLength(){
        var lastCommentTime = +(localStorage['last_new_comment_page_time_' + location.pathname.match(/^\/r\/[^\/]+\/comments\/([^\/]+)/)[1] ] || Date.now()),
            timeDiff = Date.now() - lastCommentTime;

        if(timeDiff < 300000){//< 5min
            return 30000;//30sec
        }else if(timeDiff < 600000){// < 10min
            return 60000;//60sec
        }else if(timeDiff < 600000){// < 15min
            return 90000;//90sec
        }else if(timeDiff < 1800000){// < 30min
            return 150000;//2.5 min
        }else{ // > 30 min
            return 300000; //5 min
        }
    }
})();